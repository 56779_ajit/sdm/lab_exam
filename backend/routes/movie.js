const { response } = require('express')
const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.post('/', (request, response)=>{
    const{ movie_id, movie_title, movie_release_date , movie_time,director_name} = request.body

    const query = `
        insert into Movie 
        (movie_id, movie_title, movie_release_date , movie_time,director_name)
        values
        ('${movie_id}','${movie_title}','${movie_release_date}','${movie_time}','${director_name}')
    `
    db.execute(query, (error, result)=>{
        response.send(utils.createResult(error, result))
    })
})

router.get('/', (request, response)=>{

    const{movie_title} = request.body

    const query = `
        select * from Movie
        where
        movie_title =${movie_title}
    `
    db.execute(query, (error, result)=>{
        response.send(utils.createResult(error, result))
    })
})

router.put('/movie_id', (request, response)=>{
    const{movie_id} = request.params
    const{ movie_title, movie_release_date , movie_time,director_name} = request.body
    
    const query = `
        update movie
        set movie_title  = '${movie_title}',
        set movie_release_date = '${movie_release_date}'
        set movie_time = '${movie_time}'
        set director_name = '${director_name}'
        where
        movie_id = ${movie_id}
    `
    db.execute(query, (error, result)=>{
        response.send(utils.createResult(error, result))
    })
})

router.delete('/movie_id', (request, response)=>{

    const{movie_id} = request.body

    const query = `
        delete from Movie
        where
        movie_id =${movie_id}
    `
    db.execute(query, (error, result)=>{
        response.send(utils.createResult(error, result))
    })
})



module.exports = {
    router,
}